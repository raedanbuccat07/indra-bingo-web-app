import { useState, useEffect, Fragment, useCallback, useRef } from 'react'
import { Container, Row, Col, Button, Modal, Table, Accordion, Tab, Tabs, Image } from "react-bootstrap";
import { Howl } from 'howler'
import DrumRoll from './audioclips/Drumroll.mp3'
import Tada from './audioclips/Tada.mp3'
import Swal from 'sweetalert2/dist/sweetalert2.all.min.js'
import ReactCanvasConfetti from "react-canvas-confetti"
import './App.css'
import { Scrollbars } from 'react-custom-scrollbars-2'
import * as xlsx from "xlsx";
import defaultBingoData from "./Bingo.json";
import Sparkle from 'react-sparkle';
import backgroundDefault from './images/bg.jpg';
import defaultBackground from './images/bg.jpg';
import logoImage from './images/logo.png';

const canvasStyles = {
  position: "fixed",
  pointerEvents: "none",
  width: "100%",
  height: "100%",
  top: 0,
  left: 0,
  zIndex: 999
};

function App() {
  const [bingo, setBingo] = useState(defaultBingoData)
  const [bingoShuffle, setBingoShuffle] = useState([])
  const [bingoUsed, setBingoUsed] = useState([])
  const [bingoOriginal, setBingoOriginal] = useState(defaultBingoData)
  const [firstDraw, setFirstDraw] = useState(true)
  const bingoLetters = ["B", "I", "N", "G", "O"];
  const [bingoDisplay, setBingoDisplay] = useState([])
  const [excludedNumber, setExcludedNumber] = useState([])
  const [disableDraw, setDisableDraw] = useState(false)
  const [showKeywordButton, setShowKeywordButton] = useState(false)
  const [showKeywordText, setShowKeywordText] = useState(false)
  const [modalShow, setModalShow] = useState(false)
  const refAnimationInstance = useRef(null);
  const [backgroundImage, setBackgroundImage] = useState('');
  const [backgroundBlur, setBackgroundBlur] = useState(0);
  const [enableSparkle, setEnableSparkle] = useState(true);

  const getInstance = useCallback((instance) => {
    refAnimationInstance.current = instance;
  }, []);

  const makeShot = useCallback((particleRatio, opts) => {
    refAnimationInstance.current &&
      refAnimationInstance.current({
        ...opts,
        origin: { y: 0.7 },
        particleCount: Math.floor(200 * particleRatio)
      });
  }, []);

  const fire = useCallback(() => {
    makeShot(0.25, {
      spread: 26,
      startVelocity: 55
    });

    makeShot(0.2, {
      spread: 60
    });

    makeShot(0.35, {
      spread: 100,
      decay: 0.91,
      scalar: 0.8
    });

    makeShot(0.1, {
      spread: 120,
      startVelocity: 25,
      decay: 0.92,
      scalar: 1.2
    });

    makeShot(0.1, {
      spread: 120,
      startVelocity: 45
    });
  }, [makeShot]);

  // Bingo Randomizer
  async function bingoRandomizer(randomInt) { // We need to wrap the loop into an async function for this to work
    const timer = ms => new Promise(res => setTimeout(res, ms))
    let loopRandomInt = Math.floor(Math.random() * (bingo.length - 30)) + 30

    setDisableDraw(true)
    SoundPlay(DrumRoll)

    for (let i = loopRandomInt - 29; i <= loopRandomInt; i++) {
      if (i === loopRandomInt) {
        setBingoDisplay(bingo[randomInt])
        setBingoUsed([...bingoUsed, bingo[randomInt]])
        break
      } else {
        if (!(i >= 0 && i < bingoShuffle.length)) {
          setBingoDisplay(
            {
              "Keyword": "0",
              "Letter": bingoLetters[Math.floor(Math.random() * bingoLetters.length)]
            }
          )
        } else {
          setBingoDisplay(bingoShuffle[i])
        }

        await timer(110); // then the created Promise can be awaited
      }
    }
    await timer(500);
    setShowKeywordButton(true)
  }

  // Draw Bingo
  const drawBingo = async () => {
    if (bingo.length === 0) {
      Swal.fire({
        icon: "error",
        title: "Upload Bingo Data First",
      });

      return;
    }

    var randomInt = Math.floor(Math.random() * bingo.length);
    var randomFirstInt = Math.floor(Math.random() * bingo.length);

    setShowKeywordText(false)

    switch (firstDraw) {
      case true:
        bingoRandomizer(randomFirstInt)
        setExcludedNumber([...excludedNumber, randomFirstInt])
        setFirstDraw(false)

        const newBingo = bingo.map(el => {
          if (el.isUsed === false && el.id === 1)
            return Object.assign({}, el, { isUsed: true })
          return el
        })

        setBingo(newBingo);
        break;
      default:
        do {
          if (excludedNumber.length >= bingo.length) {
            Swal.fire({
              icon: 'error',
              title: 'Bingo Reset',
              text: 'All bingo keywords has appeared. Please reset the Bingo.',
            })
            break;
          }

          if (excludedNumber.includes(randomInt)) {
            randomInt = Math.floor(Math.random() * bingo.length);
          }

          if (!excludedNumber.includes(randomInt)) {
            setExcludedNumber([...excludedNumber, randomInt])
            bingoRandomizer(randomInt)
          }

          const newBingo = bingo.map(el => {

            if (el.isUsed === false && el.id === randomInt + 1)
              return Object.assign({}, el, { isUsed: true })
            return el;
          })

          setBingo(newBingo)
        } while (excludedNumber.includes(randomInt))
    }
  }

  const bingoReset = async () => {
    Swal.fire({
      title: 'Are you sure you want to reset?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'RESET'
    }).then((result) => {
      if (result.isConfirmed) {
        setBingo(bingoOriginal)
        setExcludedNumber([])
        setFirstDraw(true)
        setBingoDisplay([])
        setBingoUsed([])
        setDisableDraw(false)
        setShowKeywordButton(false)
        setShowKeywordText(false)
        setModalShow(false)
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Reset',
          showConfirmButton: false,
          timer: 1500
        })
      }
    })
  }

  const showKeyword = async () => {
    setDisableDraw(false)
    setShowKeywordButton(false)
    fire()
    SoundPlay(Tada)
    setShowKeywordText(true)
  }

  const drawList = async () => {
    setModalShow(true)
  }

  const readUploadFile = (e) => {
    e.preventDefault();
    if (e.target.files) {
      const reader = new FileReader();
      reader.onload = (e) => {
        const data = e.target.result;
        const workbook = xlsx.read(data, { type: "array" });
        const sheetName = workbook.SheetNames[0];
        const worksheet = workbook.Sheets[sheetName];
        const json = xlsx.utils.sheet_to_json(worksheet);

        const bingoFromServer = json;
        const bingoShuffle = bingoFromServer
          .map(value => ({ value, sort: Math.random() }))
          .sort((a, b) => a.sort - b.sort)
          .map(({ value }) => value)

        setBingoShuffle(bingoShuffle)
        setBingo(bingoFromServer)
        setBingoOriginal(bingoFromServer)
      };

      reader.readAsArrayBuffer(e.target.files[0]);
    }
  }

  const addBlurBackground = () => {
    setBackgroundBlur(backgroundBlur + 1);
  };

  const reduceBlurBackground = () => {
    if (backgroundBlur <= 0) {
      return
    } else {
      setBackgroundBlur(backgroundBlur - 1);
    }
  };

  const removeBlurBackground = () => {
    setBackgroundBlur(0);
  };

  const setDefaultBackground = () => {
    setBackgroundImage(defaultBackground);
  }

  const removeBackground = () => {
    setBackgroundImage();
  }

  const turnOnSpark = () => {
    if (enableSparkle) {
      setEnableSparkle(false);
    } else if (!enableSparkle) {
      setEnableSparkle(true);
    }
  };

  useEffect(() => {
    const fileInput = document.getElementById("fileInput");
    fileInput.addEventListener("change", e => {
      const file = fileInput.files[0];
      const reader = new FileReader();
      reader.addEventListener("load", () => {
        setBackgroundImage(reader.result);
      });
      reader.readAsDataURL(file);
      document.getElementById("fileInput").value = ""
    });
  }, [])

  const SoundPlay = (src) => {
    const sound = new Howl({
      src,
      html5: true,
    })
    sound.play()
  }

  return (
    <Fragment>
      <Scrollbars
        autoHide
        autoHideTimeout={1000}
        autoHideDuration={200}
      >
        {
          enableSparkle ?
            (disableDraw ?
              (<Sparkle
                minSize={10}
                maxSize={30}
                flickerSpeed={"fast"}
                overflowPx={-10}
              />)
              :
              (<Sparkle
                minSize={1}
                maxSize={10}
                overflowPx={-10}
              />))
            :
            (<></>)
        }

        <div className="bingo-container2 w-100 h-100" style={{ backgroundImage: `url(${backgroundImage ? backgroundImage : backgroundDefault})`, filter: `blur(${backgroundBlur}px)`, WebkitFilter: `blur(${backgroundBlur}px)` }}></div>
        <Container fluid className="bingo-container h-100 m-0 p-0">
          <Row className="h-60 m-0 p-0">
            <Col className="h-50 d-flex align-items-center justify-content-center col-12 pt-5">
              {
                (bingoDisplay) ?
                  <Fragment>
                    <h1 className="text-bingo">
                      <span className="char-bingo">{bingoDisplay.Letter}</span>
                      {
                        (showKeywordText === true) ?
                          <Fragment>
                            <span> - {bingoDisplay.Keyword}</span>
                          </Fragment>
                          :
                          <span></span>
                      }
                    </h1>
                  </Fragment>
                  :
                  <></>
              }
            </Col>
            <Col className="h-40 d-flex align-items-center justify-content-center col-12">
              {
                (disableDraw === false) ?
                  <Button size="lg" className="draw-button" onClick={() => drawBingo()} active>DRAW</Button>
                  :
                  <Fragment>
                    {
                      (disableDraw === true && showKeywordButton === true) ?
                        <Button size="lg" className="draw-button" onClick={() => showKeyword()}>SHOW NUMBER</Button>
                        :
                        <></>
                    }
                  </Fragment>
              }
            </Col>
            <Col className="h-10 d-flex align-items-center justify-content-center col-12">

            </Col>
          </Row>

          <ReactCanvasConfetti refConfetti={getInstance} style={canvasStyles} />

          <Row className="h-40 m-0 p-0 d-flex align-items-end">
            <Image src={logoImage} className="bingo-logo" />
          </Row>

          <Accordion>
            <Accordion.Item eventKey="0">
              <Accordion.Header>OPTIONS (CLICK ME)</Accordion.Header>
              <Accordion.Body>
                <Tabs defaultActiveKey="file" className="mb-3">
                  <Tab eventKey="file" title="Upload File">
                    <Row>
                      <Col className="text-end">
                        <form>
                          <label
                            htmlFor="uploadData"
                            style={{
                              cursor: "pointer",
                              background: "grey",
                              padding: "5px",
                              color: "white",
                            }}
                          >
                            Upload Form Data
                          </label>
                          <input
                            className="d-none"
                            style={{ visibility: "hidden" }}
                            type="file"
                            name="uploadData"
                            id="uploadData"
                            onChange={readUploadFile}
                          />
                        </form>
                      </Col>
                      <Col className="text-start">
                        <form>
                          <label
                            htmlFor="fileInput"
                            style={{
                              cursor: "pointer",
                              background: "grey",
                              padding: "5px",
                              color: "white",
                            }}
                          >
                            Set Background Image
                          </label>
                          <input
                            className="d-none"
                            style={{ visibility: "hidden" }}
                            type="file"
                            name="fileInput"
                            id="fileInput"
                          />
                        </form>
                      </Col>
                      <Col className="text-start">
                        <Button variant="info" onClick={() => setDefaultBackground()}>Add Default Background</Button>
                      </Col>
                      <Col className="text-start">
                        <Button variant="dark" onClick={() => removeBackground()}>Remove Background</Button>
                      </Col>
                      <Col className="text-start">
                        <Button variant="success" onClick={() => addBlurBackground()}>Add Blur</Button>
                        <Button variant="primary" onClick={() => reduceBlurBackground()}>Reduce Blur</Button>
                        <Button variant="danger" onClick={() => removeBlurBackground()}>Remove Blur</Button>
                      </Col>
                      <Col xs={12} className="d-flex justify-content-center align-items-center">
                        {
                          enableSparkle ?
                            (<Button variant="danger" onClick={() => turnOnSpark()}>Turn Off Spark</Button>)
                            :
                            (<Button variant="success" onClick={() => turnOnSpark()}>Turn On Spark</Button>)
                        }
                      </Col>
                    </Row>
                  </Tab>
                  <Tab eventKey="drawlist" title="Draw List/Reset">
                    <Row>
                      <Col className="text-end">
                        <Button size="lg" variant="outline" className="draw-list-button" onClick={() => drawList()} active>DRAW LIST</Button>
                      </Col>
                      <Col className="text-start">
                        <Button size="lg" variant="outline" className="reset-button" onClick={() => bingoReset()} active>RESET</Button>
                      </Col>
                    </Row>
                  </Tab>
                </Tabs>
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </Container>
      </Scrollbars>

      <Modal
        show={modalShow}
        onHide={() => setModalShow(false)}
        size="lg"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            <h1 className="text-center">Bingo Draw List - Total Draw: {bingoUsed.length}</h1>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Table striped bordered hover variant="dark">
            <thead>
              <tr>
                <th>Letter</th>
                <th>Keyword</th>
              </tr>
            </thead>
            <tbody>
              {
                (bingoUsed.length > 0) ?
                  bingoUsed.map((obj, i) =>
                    <tr key={i}>
                      <td>{i + 1} - {obj.Letter}</td>
                      <td>{obj.Keyword}</td>
                    </tr>
                  )
                  :
                  <></>
              }
            </tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => setModalShow(false)}>Close</Button>
        </Modal.Footer>
      </Modal>
    </Fragment >
  );
}

export default App;
